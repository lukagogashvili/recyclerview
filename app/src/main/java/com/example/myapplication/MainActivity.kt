package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var recyclerViewPersonAdapter: RecyclerViewPersonAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(R.id.recyclerView)
        recyclerViewPersonAdapter = RecyclerViewPersonAdapter(getData())
        recyclerView.adapter = recyclerViewPersonAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)

    }

    private fun getData(): List<Person> {
        val personList = ArrayList<Person>()

        personList.add(
            Person(
                1,
                "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/2014_MINI_Cooper_Hardtop_--_NHTSA_test_8883_-_front.jpg/1200px-2014_MINI_Cooper_Hardtop_--_NHTSA_test_8883_-_front.jpg",
                "Gocha",
                "Gocha Vake"
            )
        )

        personList.add(
            Person(
                2,
                "https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/2019_Lamborghini_Urus_V8_Automatic_4.0_Front.jpg/1200px-2019_Lamborghini_Urus_V8_Automatic_4.0_Front.jpg",
                "Tatuka",
                "Tatu Tatuli"
            )
        )

        personList.add(
            Person(
                3,
                "https://www.auto-data.net/images/f23/BMW-X5-E70-facelift-2010.jpg",
                "Ongeri",
                "Ongeri Gulmagarashvili"
            )
        )

        personList.add(
            Person(
                4,
                "https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/images/17q3/685270/2017-porsche-911-carrera-gts-cabriolet-pdk-automatic-test-review-car-and-driver-photo-692251-s-original.jpg?fill=2:1&resize=1200:*",
                "Diko",
                "Diko Di"
            )
        )

        personList.add(
            Person(
                5,
                "https://images.honestjohn.co.uk/imagecache/file/width/640/media/6348089/Lada%20Riva%20(1).jpg",
                "Pachiko",
                "Pachiko Kalmaxashvili"
            )
        )

        personList.add(
            Person(
                1,
                "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/2014_MINI_Cooper_Hardtop_--_NHTSA_test_8883_-_front.jpg/1200px-2014_MINI_Cooper_Hardtop_--_NHTSA_test_8883_-_front.jpg",
                "Gocha",
                "Gocha Vake"
            )
        )

        personList.add(
            Person(
                2,
                "https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/2019_Lamborghini_Urus_V8_Automatic_4.0_Front.jpg/1200px-2019_Lamborghini_Urus_V8_Automatic_4.0_Front.jpg",
                "Tatuka",
                "Tatu Tatuli"
            )
        )

        personList.add(
            Person(
                3,
                "https://www.auto-data.net/images/f23/BMW-X5-E70-facelift-2010.jpg",
                "Ongeri",
                "Ongeri Gulmagarashvili"
            )
        )

        personList.add(
            Person(
                4,
                "https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/images/17q3/685270/2017-porsche-911-carrera-gts-cabriolet-pdk-automatic-test-review-car-and-driver-photo-692251-s-original.jpg?fill=2:1&resize=1200:*",
                "Diko",
                "Diko Di"
            )
        )

        personList.add(
            Person(
                5,
                "https://images.honestjohn.co.uk/imagecache/file/width/640/media/6348089/Lada%20Riva%20(1).jpg",
                "Pachiko",
                "Pachiko Kalmaxashvili"
            )
        )

        personList.add(
            Person(
                1,
                "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/2014_MINI_Cooper_Hardtop_--_NHTSA_test_8883_-_front.jpg/1200px-2014_MINI_Cooper_Hardtop_--_NHTSA_test_8883_-_front.jpg",
                "Gocha",
                "Gocha Vake"
            )
        )

        personList.add(
            Person(
                2,
                "https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/2019_Lamborghini_Urus_V8_Automatic_4.0_Front.jpg/1200px-2019_Lamborghini_Urus_V8_Automatic_4.0_Front.jpg",
                "Tatuka",
                "Tatu Tatuli"
            )
        )

        personList.add(
            Person(
                3,
                "https://www.auto-data.net/images/f23/BMW-X5-E70-facelift-2010.jpg",
                "Ongeri",
                "Ongeri Gulmagarashvili"
            )
        )

        personList.add(
            Person(
                4,
                "https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/images/17q3/685270/2017-porsche-911-carrera-gts-cabriolet-pdk-automatic-test-review-car-and-driver-photo-692251-s-original.jpg?fill=2:1&resize=1200:*",
                "Diko",
                "Diko Di"
            )
        )

        personList.add(
            Person(
                5,
                "https://images.honestjohn.co.uk/imagecache/file/width/640/media/6348089/Lada%20Riva%20(1).jpg",
                "Pachiko",
                "Pachiko Kalmaxashvili"
            )
        )

        personList.add(
            Person(
                1,
                "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/2014_MINI_Cooper_Hardtop_--_NHTSA_test_8883_-_front.jpg/1200px-2014_MINI_Cooper_Hardtop_--_NHTSA_test_8883_-_front.jpg",
                "Gocha",
                "Gocha Vake"
            )
        )

        personList.add(
            Person(
                2,
                "https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/2019_Lamborghini_Urus_V8_Automatic_4.0_Front.jpg/1200px-2019_Lamborghini_Urus_V8_Automatic_4.0_Front.jpg",
                "Tatuka",
                "Tatu Tatuli"
            )
        )

        personList.add(
            Person(
                3,
                "https://www.auto-data.net/images/f23/BMW-X5-E70-facelift-2010.jpg",
                "Ongeri",
                "Ongeri Gulmagarashvili"
            )
        )

        personList.add(
            Person(
                4,
                "https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/images/17q3/685270/2017-porsche-911-carrera-gts-cabriolet-pdk-automatic-test-review-car-and-driver-photo-692251-s-original.jpg?fill=2:1&resize=1200:*",
                "Diko",
                "Diko Di"
            )
        )

        personList.add(
            Person(
                5,
                "https://images.honestjohn.co.uk/imagecache/file/width/640/media/6348089/Lada%20Riva%20(1).jpg",
                "Pachiko",
                "Pachiko Kalmaxashvili"
            )
        )

        personList.add(
            Person(
                1,
                "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/2014_MINI_Cooper_Hardtop_--_NHTSA_test_8883_-_front.jpg/1200px-2014_MINI_Cooper_Hardtop_--_NHTSA_test_8883_-_front.jpg",
                "Gocha",
                "Gocha Vake"
            )
        )

        personList.add(
            Person(
                2,
                "https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/2019_Lamborghini_Urus_V8_Automatic_4.0_Front.jpg/1200px-2019_Lamborghini_Urus_V8_Automatic_4.0_Front.jpg",
                "Tatuka",
                "Tatu Tatuli"
            )
        )

        personList.add(
            Person(
                3,
                "https://www.auto-data.net/images/f23/BMW-X5-E70-facelift-2010.jpg",
                "Ongeri",
                "Ongeri Gulmagarashvili"
            )
        )

        personList.add(
            Person(
                4,
                "https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/images/17q3/685270/2017-porsche-911-carrera-gts-cabriolet-pdk-automatic-test-review-car-and-driver-photo-692251-s-original.jpg?fill=2:1&resize=1200:*",
                "Diko",
                "Diko Di"
            )
        )

        personList.add(
            Person(
                5,
                "https://images.honestjohn.co.uk/imagecache/file/width/640/media/6348089/Lada%20Riva%20(1).jpg",
                "Pachiko",
                "Pachiko Kalmaxashvili"
            )
        )

        personList.add(
            Person(
                1,
                "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/2014_MINI_Cooper_Hardtop_--_NHTSA_test_8883_-_front.jpg/1200px-2014_MINI_Cooper_Hardtop_--_NHTSA_test_8883_-_front.jpg",
                "Gocha",
                "Gocha Vake"
            )
        )

        personList.add(
            Person(
                2,
                "https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/2019_Lamborghini_Urus_V8_Automatic_4.0_Front.jpg/1200px-2019_Lamborghini_Urus_V8_Automatic_4.0_Front.jpg",
                "Tatuka",
                "Tatu Tatuli"
            )
        )

        personList.add(
            Person(
                3,
                "https://www.auto-data.net/images/f23/BMW-X5-E70-facelift-2010.jpg",
                "Ongeri",
                "Ongeri Gulmagarashvili"
            )
        )

        personList.add(
            Person(
                4,
                "https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/images/17q3/685270/2017-porsche-911-carrera-gts-cabriolet-pdk-automatic-test-review-car-and-driver-photo-692251-s-original.jpg?fill=2:1&resize=1200:*",
                "Diko",
                "Diko Di"
            )
        )

        personList.add(
            Person(
                5,
                "https://images.honestjohn.co.uk/imagecache/file/width/640/media/6348089/Lada%20Riva%20(1).jpg",
                "Pachiko",
                "Pachiko Kalmaxashvili"
            )
        )
        return personList

    }

}