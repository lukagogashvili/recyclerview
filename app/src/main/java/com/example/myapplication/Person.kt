package com.example.myapplication

data class Person(
    val id: Int,
    val imageUrl: String,
    val title: String,
    val name: String,
)
